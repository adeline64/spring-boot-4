package com.example.spring4.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring4.entity.Book;
import com.example.spring4.repository.BookRepository;

import lombok.Data;

@Data
@Service
public class BookService {

    @Autowired
    private BookRepository BookRepository;

    public Iterable<Book> getBooks() {
        return BookRepository.findAll();
    }

    public Optional<Book> getBook(final Long id) {
        return BookRepository.findById(id);
    }

    public void deleteBookByID(final Long id) {
        BookRepository.deleteById(id);
    }

    public Book saveOrUpdate(Book book)
    {
        return BookRepository.save(book);
    }

}

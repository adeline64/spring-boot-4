package com.example.spring4.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.example.spring4.entity.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {

}

package com.example.spring4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.spring4.entity.Book;
import com.example.spring4.service.BookService;

import java.util.Optional;

@RestController
public class BookController {

    @Autowired
    private BookService BookService;

    @GetMapping("/books")
    public Iterable<Book> getBooks() {
        return BookService.getBooks();
    }

    @GetMapping("/books/{id}")
    public Optional<Book> getContact( @PathVariable Long id) {
        return BookService.getBook(id);
    }

    @DeleteMapping("/books/{id}")
    public void deleteContactByID(@PathVariable Long id)
    {
        BookService.deleteBookByID(id);
    }

    @PostMapping("/books")
    private Book saveBook(@RequestBody Book book)
    {
        return BookService.saveOrUpdate(book);
    }

    @PutMapping("/books/{id}")
    private Book update(@RequestBody Book book)
    {
        BookService.saveOrUpdate(book);
        return book;
    }

}
